Write-Host "Congratulations! Your first script executed successfully"
# Comentario hidden

<# Powershell scripts example-hidden
autor: Jordy Peña
team: agile07
HOW TO RUN
/home/jordy/set-apimBackend.ps1 <RESOURCE GROUP> <API MANAGEMENT> <API_ID> #>

$r_group=$args[0]
echo "RESOURCE GROUP NAME: $r_group"

$apim_management=$args[1]
echo "APIM MANAGEMENT NAME: $apim_management"

$api_id=$args[2]
echo "API ID - NAME API (is not the title): $api_id"

#Get-AzApiManagement | select name
$apimContext = New-AzApiManagementContext -ResourceGroupName $r_group -ServiceName $apim_management
echo "$apimContext = New-AzApiManagementContext -ResourceGroupName $r_group -ServiceName $apim_management"

#Get-AzApiManagementApi -Context $apimContext | select name
$var1=Get-AzApiManagementApi -Context $apimContext -ApiId $api_id
echo $var1
Write-Host "========== Var2 "
$var2=Get-AzApiManagementApi -Context $apimContext -ApiId $api_id  | select ApiId,Name,Description,ServiceUrl,Path
echo $var2
Write-Host "========== var3 "
$var3=Get-AzApiManagementOperation -Context $apimContext -ApiId $api_id | select ApiId,Name,Method,UrlTemplate
echo $var3