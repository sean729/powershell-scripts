Write-Host "REBOOT API IN AKS - SCRIPT POWERSHELL"
# Comentario hidden

<# Powershell scripts example-hidden
autor: Jordy Peña
team: agile07
HOW TO RUN:
/home/jordy/r-api-aks.ps1 <RESOURCE GROUP> <AKS NAME> <API_NAME> #>

$r_group=$args[0]
echo "RESOURCE GROUP NAME: $r_group"

$aks_name=$args[1]
echo "AKS NAME: $aks_name"

$api_name=$args[2]
echo "API NAME: $api_name"

Write-Host "=========================================================================================="

# Login AKS
az aks get-credentials --resource-group $r_group --name $aks_name --admin

# get pods
kubectl -n atla get pods | grep $api_name
$pod_name=kubectl -n atla get pods | grep $api_name | awk '{print $1}'
echo $pod_name

# reboot pod_name
echo "kubectl -n atla delete pod $pod_name"
#kubectl -n atla delete pod $pod_name
$pod_name=kubectl -n atla get pods | grep $api_name | awk '{print $1}'

# watch
watch "kubectl -n atla get pods | grep $pod_name"